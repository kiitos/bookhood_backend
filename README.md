# BookHood Backend
Technology:

- Python Version 3.8.5
- sqlite3 (current)
- PostgreSQL 11 (future)
- Django 3.1.6
Dev Environment: https://bh-be-dev.herokuapp.com/

Prod Environment: https://bh-be-prod.herokuapp.com/
# Set up
1. From the root of your project run using `python3`:
```
python -m venv .venv
```

2. Activate the virtual env:
```
source .venv/bin/activate
```

3. Install requirements:
```
pip install -r bookhood_backend/requirements.txt
```
4. Install GDAL:

Mac:
```
brew install gdal
```
Windows:
```
TODO: ADD windows installing instructions
```

5. Start project
```
cd bookhood_backend
python manage.py runserver
```

# Workflow
1. Branch off from the `dev` branch.
2. Do your commits, make sure that you clean up your migrations and add your installed pip packages
3. Create the PR with `dev` as the target branch.


# Useful Docker commands
Connect to DB
docker exec -it bookhood_backend_db_1 psql -U django

Connect to python
docker exec -it bookhood_backend_web_1 /bin/bash