from django.urls import include, path
from rest_framework import routers
from .views import BadgeViewSet

router = routers.DefaultRouter()
router.register(r'', BadgeViewSet)

urlpatterns = router.urls

