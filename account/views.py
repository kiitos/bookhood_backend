from django.shortcuts import render
from .serializers import BadgeSerializer
from .models import Badge
from rest_framework import viewsets


# Create your views here

class BadgeViewSet(viewsets.ModelViewSet):
    queryset = Badge.objects.all()
    serializer_class = BadgeSerializer
