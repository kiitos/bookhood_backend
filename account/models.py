from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from books.models import Book


# Create your models here.
class Badge(models.Model):
    """Badge Model"""

    name = models.CharField(max_length=20)
    description = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Person(AbstractUser):
    name = models.CharField(max_length=100)
    phone_number = PhoneNumberField(null=True, blank=True, unique=True)
    email = models.EmailField(unique=True)
    active_badge = models.ForeignKey(Badge, related_name="active_badge", null=True, blank=True,
                                     on_delete=models.CASCADE)
    badges = models.ManyToManyField(Badge, blank=True)
    following = models.ManyToManyField("self", blank=True)
    library = models.ManyToManyField(Book, blank=True)
    credits = models.IntegerField(default=0)
    bio = models.TextField(max_length=300, null=True, blank=True)
    profile_pic = models.ImageField(upload_to='profile_pics', blank=True)
    address = models.CharField(max_length=200, blank=True, null=True)

    def save(self, *args, **kwargs):
        # This helps in keeping phone number unique and optional
        # Empty strings are not unique, but we can save multiple NULLs
        if not self.phone_number:
            self.phone_number = None

        super().save(*args, **kwargs)
