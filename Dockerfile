FROM daveouds/python-gdal-postgres:latest

# set work directory
WORKDIR /usr/src/bookhood_backend

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apt-get install -y libpq-dev gcc \
    &&  pip install --upgrade pip


# copy project
COPY . .

# /usr/local/bin/python -m pip install --upgrade pip
RUN pip install -r requirements.txt

# run entrypoint.sh
CMD ["sh /usr/src/bookhood_backend/bin/entrypoint.sh"]