from django.db import models
from django.contrib.gis.db.models import PointField

# Create your models here.


class Book(models.Model):
    isbn = models.CharField(max_length=13, unique=True)
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    language = models.CharField(max_length=100)
    credits = models.IntegerField()
    year = models.IntegerField()
    location = PointField()
    description = models.TextField(
        max_length=200, help_text='Enter a brief description of the book')
    isPrivate = models.BooleanField(default=False)
    # Owner = models.ForeignKey(Person, on_delete=models.CASCADE)
    # Category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
