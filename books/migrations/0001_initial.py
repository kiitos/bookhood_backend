# Generated by Django 3.1.7 on 2021-03-12 07:24

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('isbn', models.CharField(max_length=13, unique=True)),
                ('title', models.CharField(max_length=100)),
                ('author', models.CharField(max_length=100)),
                ('language', models.CharField(max_length=100)),
                ('credits', models.IntegerField()),
                ('year', models.IntegerField()),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('description', models.TextField(help_text='Enter a brief description of the book', max_length=200)),
                ('isPrivate', models.BooleanField(default=False)),
            ],
        ),
    ]
